<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
  'post_type' => array( 'member' ),
  'posts_per_page' => 10,
);
// $args['search_filter_id'] = 230;

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) : 

?>

<ul class="members">

  <?php
  while ( $query->have_posts() ) :
    
    $query->the_post();
    
    // Taxonomy - TEC Community
    $tec_community_terms = get_the_terms( $post->ID , 'tec_community' );
    
    // reset data
    $tec_community       = null;
    // $authors      = null;

    if ( ! empty( $tec_community_terms ) && ! is_wp_error( $tec_community_terms ) ):   
      foreach ( $tec_community_terms as $term ) {
        $tec_community     = $term->name;
      }
    endif;

    // Taxonomy - Region
    $region_terms = get_the_terms( $post->ID , 'region' );
    
    // reset data
    $region       = null;
    // $authors      = null;

    if ( ! empty( $region_terms ) && ! is_wp_error( $region_terms ) ):   
      foreach ( $region_terms as $term ) {
        $region     = $term->name;
      }
    endif;

    // Taxonomy - Diocese
    $diocese_terms = get_the_terms( $post->ID , 'diocese' );
    
    // reset data
    $diocese       = null;
    // $authors      = null;

    if ( ! empty( $diocese_terms ) && ! is_wp_error( $diocese_terms ) ):   
      foreach ( $diocese_terms as $term ) {
        $diocese     = $term->name;
      }
    endif;

    // Taxonomy - Roles
    $role_terms = get_the_terms( $post->ID , 'role' );
    
    // reset data
    $role       = null;
    $roles      = null;

    if ( ! empty( $role_terms ) && ! is_wp_error( $role_terms ) ):   
      foreach ( $role_terms as $term ) {
        $role     = $term->name;
      }
      if ( count($role_terms) > 1 ):
        $role_label = 'Roles';
      else:
        $role_label = 'Role';
      endif;
    endif;


    // vars
    $salutation = get_field('salutation');
    $first_name = get_field('first_name');
    $last_name = get_field('last_name');
    $casual_name = get_field('casual_name');
    $primary_email = get_field('primary_email');
    $secondary_email = get_field('secondary_email');
    $phone = get_field('phone');
    $phone_other = get_field('phone_other');
    $street_address = get_field('street_address');
    $city = get_field('city');
    $us_state = get_field('us_state');
    $zip = get_field('zip');
    $country = get_field('country');

  
    // The Loop
  ?>
  
      <li class="profile">
        <div class="profile-image-wrapper">
          <?php 
          if ( has_post_thumbnail() ):
            echo get_the_post_thumbnail( $page->ID, 'thumbnail' );
          else:
            echo '<img src="https://via.placeholder.com/150" />';
          endif;
          ?>
        </div>
        <div class="profile-desc">
          <div class="profile-desc--header">
            <h5 class="name">
              <?php
              if ( ! empty( $salutation ) ):
                echo $salutation . ' ';
              endif;
              if ( ! empty( $first_name ) ):
                echo $first_name . ' ';
              endif;
              if ( ! empty( $last_name ) ):
                echo $last_name;
              endif;
              ?>
            </h5>
          </div>
          <div class="profile-desc--body">
            <div>
              <h6 class="title">Diocese</h6>
              <p>
                <?php
                if ( ! empty( $diocese ) ):
                  echo $diocese;
                else:
                  echo '-';
                endif;
                ?>
              </p>
              <h6 class="title">TEC Community</h6>
              <p>
                <?php
                if ( ! empty( $tec_community ) ):
                  echo $tec_community;
                else:
                  echo '-';
                endif;
                ?>
              </p>
            </div>
            <div>
              <h6 class="title">Region/Country</h6>
              <p>
                <?php
                if ( ! empty( $region ) ):
                  echo $region;
                else:
                  echo '-';
                endif;
                ?>
              </p>
              <h6 class="title"><?php echo $role_label; ?></h6>
                <?php
                if ( ! empty( $role_terms ) && ! is_wp_error( $role_terms ) ):   
                  foreach ( $role_terms as $term ) {
                    $role     = $term->name;
                    echo '<p>' . $role . '</p>';
                  }
                else:
                  echo '-';
                endif;
                ?>
            </div>
          </div>
        </div>
      </li>

  <?php endwhile;?>

</ul>

<?php wp_reset_postdata(); ?>

<?php else : ?>
  <div class="nothing-found">
    <h2>Nothing Found</h2>
    <div class="alert alert-info">
      <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
    </div>
  </div>
<?php endif; ?>
  

<?php 
$total_pages = $query->max_num_pages;
if ( $total_pages > 1 ): 
?>

<div class="pagination">

  <?php
  $left_arrow = '<?xml version="1.0" encoding="UTF-8"?><svg class="left-arrow" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><defs><style>.cls-2 {fill: none;stroke: #272727;stroke-width: 5px;}</style></defs><g transform="translate(-109.62 -1391)" data-name="Group 591"><path class="cls-2" transform="translate(634.12 804.5)" d="m-462 612l-25 25 25 25" data-name="Path 2180"/></g></svg>';
  $right_arrow = '<?xml version="1.0" encoding="UTF-8"?><svg class="right-arrow" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><defs><style>.cls-2 {fill: none;stroke: #272727;stroke-width: 5px;}</style></defs><g transform="translate(-109.62 -1391)" data-name="Group 591"><path class="cls-2" transform="translate(634.12 804.5)" d="m-462 612l-25 25 25 25" data-name="Path 2180"/></g></svg>';
  ?>
  
  <div class="nav-next"><?php previous_posts_link( $left_arrow ); ?></div>
  <div class="page-bullet-wrapper">
    <?php
    for ( $i = 1; $i <= $total_pages; $i++ ) {
      echo '<a href="?sf_paged=' . $i . '">';
      echo '<div class="page-bullet';
      if ( $i == $paged ): 
        echo ' page-bullet-current';
      endif;
      echo '" data-page="' . $i . '"></div>';
      echo '</a>';
      // using jQuery after AJAX requests
    }
    ?>
  </div>
  <div class="nav-previous"><?php next_posts_link( $right_arrow, $query->max_num_pages ); ?></div>
  
</div>
<?php endif; ?>