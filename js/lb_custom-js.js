var $ = jQuery.noConflict();

/*********************
ANONYMOUS FUNCTIONS
*********************/
// fire functions when document is ready
$(document).ready(function() {
    // I'll take all the help I can get 
    console.log("✝JMJ");

    lb_slick();
    lb_iphonetap_fix();
    lb_smooth_scroll();
    lb_mobile_menu();
    lb_modal();
    lb_whyTec_slick();
    lb_search();
    lb_tabs();
    lb_bishops_slick();
    lb_donateCheckboxes();
    lb_communities();
    lb_donate();
    partnersMinistryReadMore('.page-id-74 .lb-two-col-text');
    accordion('.accordion .accordion-title');
    // lb_weekendCal_header();
    // open_links();
    // lb_intranet();
    $('#intranet-calendar-wrapper').intranetCalendar();
    speakersInit();
    videosInit();
    YTApiInit();
});

/*********************
DECLARED FUNCTIONS
*********************/
function lb_iphonetap_fix() {
    $('#ct_columns_12_post_62 .ct-column').attr('ontouchstart', '');
}
function open_links() {
    $("a[href^='http://']").attr("target","_blank");
}

// function accordion(element) {
//     accordionTabs = $(element);

//     $(accordionTabs).click(function(){
//         tabContent = $(this).next();

//         // $('.accordion-title').each(function(){
//         //  $(this).removeClass('active');
//         //  $(this).next().removeClass('active').css('max-height', '0px');
//         // });

//         $(this).toggleClass('active');
//         $(tabContent).toggleClass('active');

//         if($(tabContent).css('max-height') == '0px') {
//             $(tabContent).css('max-height', $(tabContent).prop('scrollHeight') + 'px');
//         } else {
//             $(tabContent).css('max-height', '0px');
//         }
//     });
// }

function lb_donate() {
    $('input#input_7_1_other').change(function() {
     var value = $(this).val();
     console.log(value);
     $('#choice_7_1_4').attr('value', '$' + value);
 });
}
function accordion(element) {
    accordionTabs = $(element);

    $(accordionTabs).click(function(){
        tabContent = $(this).next();

        $(this).toggleClass('active');
        $(tabContent).toggleClass('active');
        
        $(this).closest('.accordion').siblings().children('.accordion-body').slideUp('fast');
        $(tabContent).slideToggle('fast');
    });
}

function partnersMinistryReadMore(ele) {
    $(ele).each(function(){
        var headingEle = $(this).find('.ct-headline').html();
        var textEle = $(this).find('.ct-text-block.read-more').html();
        var contact = $(this).find('.ct-text-block.contact').html();
        var bgColor = $(this).css('background-color');
        console.log(bgColor);

        $(this).prepend('<div class="read-more-info"><div class="read-more-info-inner"><h3>' + headingEle + '</h3><p>' + textEle + '</p></div><div class="close">X Close</div></div>');
        $(this).find('.ct-text-block.contact').before('<div class="contact-info" style="background-color: '+bgColor+';"><h3 class="heading">' + headingEle + '</h3><div class="back"><< Back</div><div class="contact-info-inner"><p>' + contact + '</p></div></div>');
    });

    $('.button-wrapper .button').click(function(e){ 
        e.preventDefault();
        var buttonHref = $(this).attr('href').slice(1);

        $(this).closest('.lb-two-col-text').find('.' + buttonHref + '-info').toggleClass('active');
    });

    $('.read-more-info .close').click(function(){
        $(this).closest('.read-more-info').toggleClass('active');
    });

    $('.contact-info .back').click(function(){
        $(this).closest('.contact-info').toggleClass('active');
    });
}

function lb_slick() {
    // testimonies slick slider
    $('.lb-testimonies').slick({
        infinite: true, 
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-next"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-red-left.svg" class="arrow" scale="0"></div>', //replace arrows 
        nextArrow: '<div class="slick-prev"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-red-right.svg" class="arrow" scale="0"></div>', //replace arrows
        // repsonsive edits
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.lb-test-bishops').slick({
        infinite: true, 
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-next"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-white-left.svg" class="arrow" scale="0"></div>', //replace arrows
        prevArrow: '<div class="slick-prev"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-white-right.svg" class="arrow" scale="0"></div>', //replace arrows
    });
}

function lb_whyTec_slick() {
    $('#ct_div_block_108_post_62').slick({
        infinite: true, 
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-next"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-white-left.svg" class="arrow" scale="0"></div>', //replace arrows
        prevArrow: '<div class="slick-prev"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-white-right.svg" class="arrow" scale="0"></div>', //replace arrows

        // repsonsive edits
        responsive: [{
            breakpoint: 1366,
            settings: {
                slidesToShow: 1
            }
        }]
    });
}

function lb_bishops_slick() {
    $('#ct_code_block_27_post_70 ul').slick({
        infinite: true, 
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-next"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-red-left.svg" class="arrow" scale="0"></div>', //replace arrows
        nextArrow: '<div class="slick-prev"><img alt="left arrow" src="/wp-content/uploads/2018/03/arrow-red-right.svg" class="arrow" scale="0"></div>', //replace arrows
    });
}

function lb_donateCheckboxes() {

    $('#ct_code_block_47_post_166 input[type="checkbox"]').on('change', function() {
        $('#ct_code_block_47_post_166 input[type="checkbox"]').not(this).prop('checked', false);
    });
}

function lb_tabs() {

    if ( $('body').hasClass('blog') ) {

        $('.lb-tab-nav a').first().addClass("active");

    } else if ( $('body').hasClass('archive') ) {

        var url = $(location).attr('href'),
        parts = url.split("/"),
        last_part = parts[parts.length-2];

        $('.lb-tab-nav a#' + last_part).addClass('active');
        console.log( last_part );

    } else {

        // add class to first link item
        $('.lb-tab-nav a').first().addClass("active");

        // click function
        $('.lb-tab-nav a').click(function(e) {
            e.preventDefault();
            var lbHref = $(this).attr('href').substr(1);
            // console.log("yes");

            // remove active class
            $('.lb-tab-nav a').each(function() {
                $(this).removeClass('active');
            });
            // add to link clicked
            $(this).addClass('active');

            // remove active class
            $('.lb-tab-body').each(function() {
                $(this).removeClass('active');
            });
            // add to body relative to link clicked
            $('.lb-tab-body.' + lbHref).addClass('active');
        });
    }
}

function lb_smooth_scroll() {
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
                ) {
                // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
    }

    function lb_mobile_menu() {
        $('.lb-mobile-btn').click(function (e) {
            $('header').toggleClass('active');
            $(this).toggleClass('active');
            $('.lb-mobile-menu').toggleClass('active');
            $('body').toggleClass('menu-active');

            if (!$('.lb-mobile-menu').hasClass('active')) {
                $('#menu-main-1 li.menu-item-has-children').each(function () {
                    $(this).removeClass('active').siblings(".lb-subnav").slideUp();
                    $(this).parent().removeClass('active');
                });
            }
        });


        $('.lb-mobile-menu li.menu-item-has-children').click(function (e) {
            $(this).toggleClass('active').children(".sub-menu").slideToggle();
            $(this).parent().toggleClass('active');
            e.stopPropagation();

            console.log('dank');
        }); 
    }

    function lb_search() {
        $(function() {
            $("form #searchinput").keypress(function (e) {
                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    var searchterm = $("form #searchinput").val();
                    console.log(searchterm);
                    window.location = "/?s=" + searchterm;
                    $('button[type=submit]').click();
                    return false;
                } else {
                    return true;
                }
            });
        });
    }

    function lb_modal() {
    // click off modal to close
    $(window).click(function() {
        //Hide the menus if visible
        if ($('body').hasClass('popup-open') === true) {
            console.log("fade away");
            $('.popup').fadeOut(350);
            $('body').removeClass('popup-open');
        }
    });

    // stop propgation
    $('[data-popup-open]').click(function(event) {
        event.stopPropagation();

    });

    // stop propgation
    $('.popup-inner').click(function(event) {
        event.stopPropagation();

    });


    $(function() {
        //----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            // console.log("Open!");
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            $("body").addClass("popup-open");
            e.preventDefault();

            if($('body').hasClass('page-id-68')) {
                var sendToEmail = $(this).closest('.lb-info').find('.email').text();
                var sendToName = $(this).closest('.lb-info').find('h3').text();

                $('.popup-inner h3 span').text(sendToName);
                $('.popup-inner #input_6_5').val(sendToEmail);
            }
        });

        //----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('.popup').fadeOut(350);
            console.log("close!");
            $("body").removeClass("popup-open");

            e.preventDefault();
        });
    });
}


function lb_communities() {
    $('.prettyFileBar').prepend('<h3>Find a TEC Community</h3>');
    $('.infowindowContent a').addClass('button');
    $('.viewLocationPage').html('Visit Website');
}

function lb_weekendCal_header() {
    $('.page-id-76 #tribe-events-header').append('<h1 style="text-transform: uppercase;">TEC Calendar</h1>');
    // $('.tribe-events-nav-previous > a').click(function(event) {
    //     event.preventDefault();
    //     console.log('test');
    //     var link = $(this).attr('href');
    //     window.location = link;

    //     // var month = $(this).attr('data-month');
    //     // var url = "https://" + window.location.hostname + window.location.pathname + '?tribe-bar-date=' + month + '#tribe-events';
    // });
}

function lb_update_status_alert() {
    uri = document.referrer;

    getUrl = window.location;
    baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/";

    // strip referrer of query string
    if (uri.indexOf("?") > 0) {
        clean_uri = uri.substring(0, uri.indexOf("?"));
    } else {
        clean_uri = uri;
    }

    // If coming from Edit Profile page and not refreshing
    if ( clean_uri === baseUrl + "profile/edit/" && performance.navigation.type != 1) {
        // show alert
        $('.update-status').addClass('show');

        window.setTimeout(function() {
            $('.update-status').removeClass('show');
        }, 5000);

        $('.update-status .close').click(function() {
            $(this).closest('.update-status').removeClass('show');
        })
    } else {
        // hide alert
        $('.update-status').removeClass('show');
    }
}


$.fn.intranetCalendar = function() {
    this.each(function(){
        var calendar = $(this);

        disable_link(calendar);

        $( tribe_ev.events ).on( 'tribe_ev_ajaxSuccess', function() {
            disable_link(calendar);
        });

        function disable_link(e) {
            // unwrap <a>
            e.find('.tribe-event-url').contents().unwrap();
            // wrap with <span> using the same class, for consistency
            e.find('.tribe-events-list-event-title').contents().wrap('<span class="tribe-event-url"></span>');
        }

    });  
}

function speakersInit() {
    speakersLayout('.speakers');
    speakersReadMore('.speaker__text');

    $(window).resize(function() {
        speakersLayout('.speakers');
    });
}

function speakersLayout(ele) {

    if (window.matchMedia('(max-width: 1024px)').matches) {
        // remove columns
        $(ele).find('.speaker-column > .speaker').unwrap();
    } else if ( ! $(ele).find('.speaker-column').length ) {
        // add columns
        wrapper = '<div class="speaker-column"></div>';
        count = $(ele).find('.speaker').length;
        n = Math.ceil(count / 2);
        $(ele).children('.speaker').slice(0, n).wrapAll(wrapper);
        $(ele).children('.speaker').wrapAll(wrapper);
    }
}

function speakersReadMore(ele) {
    $(ele).each(function(){
        $(this).find('.read-more-less').click(function(e){ 
            e.preventDefault();
            // var buttonHref = $(this).attr('href').slice(1);

            textEle = $(this).closest('.speaker').find('.speaker__bio');
            textReadMore = 'Read More +';
            textReadLess = 'View Less –';

            if (textEle.hasClass('unclamped')) {
                textEle.removeClass('show-text');
                setTimeout(function() {
                  textEle.removeClass('unclamped');
              }, 500);
                $(this).text(textReadMore);
            } else {
                textEle.addClass('unclamped').addClass('show-text');
                $(this).text(textReadLess);
            }        
        });
    });
}

function videosInit() {

    setupModal();
    videosFilter();
    hideVideos();
    setVideosStyles();

    function setupModal() {
        var trigger = $('a[data-modal]');
        trigger.click(function(e) {
            e.preventDefault();

            var target      = $(this).attr('href');
            var video       = $(this).closest('.video');
            var title       = video.data('title') || video.find('.video__meta__title').text();
            var subtitle    = video.data('subtitle') || video.find('.subtitle').text();
            var published   = video.data('published') || video.find('.published-date').text();
            var duration    = video.data('duration') || video.find('.time').text();

            if ( subtitle.length ) {
                subtitle = subtitle + ' | ';
            }

            if ( published.length ) {
                published = published + ' | ';
            }

            inner    = '<div class="modal__container"><div class="iframe-wrap"><iframe frameborder="0" allowfullscreen allow="autoplay; fullscreen" src="' + target + '"/></div><div class="modal__meta"><h4 class="modal__meta__title">' + title + '</h4><div class="modal__meta__subtitle">' + subtitle + published + duration + '</div></div></div>';
            template = '<div id="modal" class="modal">' + inner + '</div>';

            if ( $('#modal').length ) {
                $('#modal').append(inner);
            } else {
                $('body').append(template);
                setupModalClose();
            }
        });
    }

    function setupModalClose() {
        $('#modal').click(function(e) {
            if( !$(e.target).closest('.modal__container').length && !$(e.target).is('.modal__container')) {
                closeModal();
            }     
        });
    }

    function closeModal() {
        $('#modal, #modal .modal__container')
            .fadeTo( 500 , 0, function() {
                // Animation complete.
                $(this).detach();
            });
    }

    function videosFilter() {
        var search = $('#search-videos');

        $(search).each(function(){

            var item = $('.videos__wrapper .video');

            $(this).on("keyup", function() {
                var value = $(this).val().toLowerCase();
                item.filter(function() {
                    if ( $(this).text().toLowerCase().indexOf(value) > -1 || ! value.length ) {
                        // matched
                        $(this).show().addClass('filtered');
                    } else {
                        // not matched
                        $(this).hide().removeClass('filtered');
                    }
                });

                // remove heading if no child is filtered
                $('.videos__wrapper').each(function() {

                    var playlist = $(this).closest('.playlist__wrapper');

                    if ( $(this).find('.video.filtered').length ) {
                        // exist
                        playlist.show();
                    } else {
                        // nothing
                        playlist.hide();
                    }
                });

                // view more / less function
                if ( $(this).val().length ) {
                    // Remove view more / less function
                    // $('.view-more').css({ 'display': 'grid' }).removeClass('hidden');
                    $('.view-more').children('.video').unwrap();
                    $('.view-more-link__wrapper').detach();
                } else {
                    // $('.view-more').css({ 'display': 'none' }).addClass('hidden');
                    item.removeClass('filtered');
                    hideVideos();
                }

            });
        });
    }

    function hideVideos() {

        wrapVideos();

        $(window).resize(function() {
            wrapVideos();
        });
    }

    function wrapVideos() {

        var ele = $('.videos__wrapper');
        var columns = getColumns();

        $(ele).each(function(){

            var video   = $(this).find('.video');
            var wrapper = $(this).closest('.playlist__wrapper');
            var link    = wrapper.find('.view-more-link');

            var count   = video.length;

            if ( count > columns ) {

                // if amount of hidden videos need to change
                var hiddenCount     = $(this).find('.view-more .video').length;
                var visibleCount    = count - hiddenCount;

                if ( columns !== visibleCount ) {
                    $(this).find('.view-more .video').unwrap(); // unwrap first
                }

                // wrap hidden videos
                if ( ! $(this).find('.view-more').length && ! $('.search-videos').val().length ) {
                    video.slice(columns,50).wrapAll('<div class="view-more hidden" style="display:none;"></div>');
                }

                var textMore = 'View More +';
                var textLess = 'View Less –';

                // if link doesn't exist
                if ( ! link.length && ! $('.search-videos').val().length ) {

                    // create link
                    wrapper.append('<div class="view-more-link__wrapper"><a class="view-more-link" href="#!">' + textMore + '</a></div>')
                    
                    // attach click event to the link
                    wrapper.find('.view-more-link').click(function(e) {

                        e.preventDefault();

                        var hidden          = wrapper.find('.view-more');
                        var hiddenHeight    = hidden.scrollHeight;
                        
                        
                        if ( hidden.hasClass('hidden') ) {
                            // show / expand
                            hidden.removeClass('hidden');
                            $(this).text(textLess);
                        } else {
                            // hide / collapse
                            hidden.addClass('hidden');
                            $(this).text(textMore);
                        }

                        hidden.animate({
                            height: "toggle",
                        }, 250);

                    });
                } else {
                    if ( $('.view-more').hasClass('hidden') ) {
                        link.text(textMore);
                    } else {
                        link.text(textLess);
                    }
                }

            } else { // count < columns
                $(this).find('.view-more .video').unwrap();
                wrapper.find('.view-more-link__wrapper').detach();
            }
        });
    }

    function getColumns() {
        var columns = 4;
        if (window.matchMedia('(max-width: 1024px)').matches) {
            var columns = 2;
        }
        if (window.matchMedia('(max-width: 600px)').matches) {
            var columns = 1;
        }
        return columns;
    }

    function setVideosStyles() {
        var ele = $('.videos__body');
        var styles = ["overlay--yellow", "overlay--red", "overlay--blue"];
        var index = 0;
        $(ele).find(".video").each(function() {
            $(this).find('.video__overlay').addClass(styles[index++]);
            if (index >= styles.length) {
                index = 0;
            }
        });
    }
}

//
// VIDEO API

function YTApiInit() {
    
    // initiate placeholder / preloader
    videoPlaceholderInit();
    YTApiSetup();

    function videoPlaceholderInit() {
        $('.video').each(function() {
            $(this).addClass('data-loading');
        });
    }

    function YTApiSetup() {
        $(window).on('load', function() {
            $('.video').each(function() {
                getYTApiData($(this));
            }).promise().done(function(){ 
                $('.video').removeClass('data-loading');
            });
        });
    }

    function getYTApiData(ele) {
        var yt_video_id     = ele.data('video-id');
        var yt_api_key      = "AIzaSyC2bZ16qpDX9jb2VDbhb1y2cqIKOk5Z_cM";

        // pre-defined custom data
        var custom_title    = ele.data('title');
        
        var fields          = "items(id,snippet(title),contentDetails(duration),snippet(publishedAt))";
        var part            = "snippet,contentDetails";
        var yt_api_endpoint = "https://www.googleapis.com/youtube/v3/videos?part="+part+"&fields="+fields+"&id="+yt_video_id+"&key="+yt_api_key;
        
        
        
        var jqxhr = $.getJSON(yt_api_endpoint)
            .done(function(data) {

                // get vars
                var title       = custom_title || getTitle(data);
                var publishedAt = getPublishedAt(data);
                var duration    = getDuration(data);

                // return data
                data = [];
                data["title"] = title;
                data["publishedAt"] = publishedAt;
                data["duration"] = duration;

                populateYTVideoData(ele, data);

            })
            .fail(function() {
                console.log("error, see network tab for response details");
                populateYTVideoData(ele);
                // return false;
            });


        function getTitle(snippet_json_data){
            var title = snippet_json_data.items[0].snippet.title;
            return title;
        }

        function getPublishedAt(snippet_json_data){
            var publishedAt = snippet_json_data.items[0].snippet.publishedAt;
            return publishedAt;
        }

        function getDuration(snippet_json_data){
            var duration = snippet_json_data.items[0].contentDetails.duration;
            return duration;
        }
    }

    function YTDurationConvert(duration) {
        var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

        match = match.slice(1).map(function(x) {
            if (x != null) {
                return x.replace(/\D/, '');
            }
        });

        var hours = (parseInt(match[0]) || 0).toString();
        var minutes = (parseInt(match[1]) || 0).toString();
        var seconds = (parseInt(match[2]) || 0).toString();

        if (seconds.length == 1) {
            var seconds = '0' + seconds;
        } 
        if (minutes.length == 1) {
            var minutes = '0' + minutes;
        } 

        if ( hours < 1 ) {
            return minutes + ':' + seconds;
        } else {
            return hours + ':' + minutes + ':' + seconds;
        }
    }

    function populateYTVideoData(ele, data) {

        // ele = Youtube ID element
        // data = data to populate

        if ( data ) {
            var duration    = YTDurationConvert(data["duration"]);
            var publishedAt = $.datepicker.formatDate('d M yy', new Date(data["publishedAt"]));

            ele.find('.time').text(duration);
            ele.find('.published-date').text(publishedAt);
        }

        var title       = ele.find('.video__meta__title').text() || data["title"];
        var src         = ele.find('.video__image').data("src");

        ele.find('.video__image').attr('src', src);
        ele.find('.video__meta__title').text(title);

    }
}
