<?php 

// Register Custom Post Type
function testimony() {

    $labels = array(
        'name'                  => _x( 'Testimonies', 'Post Type General Name', 'tec' ),
        'singular_name'         => _x( 'Testimony', 'Post Type Singular Name', 'tec' ),
        'menu_name'             => __( 'Testimonies', 'tec' ),
        'name_admin_bar'        => __( 'Testimonies', 'tec' ),
        'archives'              => __( 'Testimony Archives', 'tec' ),
        'attributes'            => __( 'Testimony Attributes', 'tec' ),
        'parent_item_colon'     => __( 'Parent Testimony:', 'tec' ),
        'all_items'             => __( 'All Testimonies', 'tec' ),
        'add_new_item'          => __( 'Add New Testimony', 'tec' ),
        'add_new'               => __( 'Add New', 'tec' ),
        'new_item'              => __( 'New Testimony', 'tec' ),
        'edit_item'             => __( 'Edit Testimony', 'tec' ),
        'update_item'           => __( 'Update Testimony', 'tec' ),
        'view_item'             => __( 'View Testimony', 'tec' ),
        'view_items'            => __( 'View Testimonies', 'tec' ),
        'search_items'          => __( 'Search Testimonies', 'tec' ),
        'not_found'             => __( 'Not found', 'tec' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'tec' ),
        'featured_image'        => __( 'Featured Image', 'tec' ),
        'set_featured_image'    => __( 'Set featured image', 'tec' ),
        'remove_featured_image' => __( 'Remove featured image', 'tec' ),
        'use_featured_image'    => __( 'Use as featured image', 'tec' ),
        'insert_into_item'      => __( 'Insert into Testimony', 'tec' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Testimony', 'tec' ),
        'items_list'            => __( 'Testimony list', 'tec' ),
        'items_list_navigation' => __( 'Testimony list navigation', 'tec' ),
        'filter_items_list'     => __( 'Filter Testimony list', 'tec' ),
    );
    $args = array(
        'label'                 => __( 'Testimonies', 'tec' ),
        'description'           => __( 'Testimonies part of TEC' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 2,
        'menu_icon'             => 'dashicons-admin-comments',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'testimony', $args );

}
add_action( 'init', 'testimony', 0 );

add_action( 'init', 'gifts_supplies', 0 );
// Register Custom Post Type
function gifts_supplies() {

    $labels = array(
        'name'                  => _x( 'Gifts & Supplies', 'Post Type General Name', 'tec' ),
        'singular_name'         => _x( 'Gifts & Supplies', 'Post Type Singular Name', 'tec' ),
        'menu_name'             => __( 'Gifts & Supplies', 'tec' ),
        'name_admin_bar'        => __( 'Gifts & Supplies', 'tec' ),
        'archives'              => __( 'Gifts & Supplies Archives', 'tec' ),
        'attributes'            => __( 'Gifts & Supplies Attributes', 'tec' ),
        'parent_item_colon'     => __( 'Parent Gifts & Supplies:', 'tec' ),
        'all_items'             => __( 'All Gifts & Supplies', 'tec' ),
        'add_new_item'          => __( 'Add New Gifts & Supplies', 'tec' ),
        'add_new'               => __( 'Add New', 'tec' ),
        'new_item'              => __( 'New Gifts & Supplies', 'tec' ),
        'edit_item'             => __( 'Edit Gifts & Supplies', 'tec' ),
        'update_item'           => __( 'Update Gifts & Supplies', 'tec' ),
        'view_item'             => __( 'View Gifts & Supplies', 'tec' ),
        'view_items'            => __( 'View Gifts & Supplies', 'tec' ),
        'search_items'          => __( 'Search Gifts & Supplies', 'tec' ),
        'not_found'             => __( 'Not found', 'tec' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'tec' ),
        'featured_image'        => __( 'Featured Image', 'tec' ),
        'set_featured_image'    => __( 'Set featured image', 'tec' ),
        'remove_featured_image' => __( 'Remove featured image', 'tec' ),
        'use_featured_image'    => __( 'Use as featured image', 'tec' ),
        'insert_into_item'      => __( 'Insert into Gifts & Supplies', 'tec' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Gifts & Supplies', 'tec' ),
        'items_list'            => __( 'Gifts & Supplies list', 'tec' ),
        'items_list_navigation' => __( 'Gifts & Supplies list navigation', 'tec' ),
        'filter_items_list'     => __( 'Filter Gifts & Supplies list', 'tec' ),
    );
    $args = array(
        'label'                 => __( 'Gifts & Supplies', 'tec' ),
        'description'           => __( 'Gifts & Supplies of TEC' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'product' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'menu_icon'             => 'dashicons-cart',
        'show_in_admin_bar'     => true,
        'publicly_queryable'    => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'gifts_supplies', $args );

}



// create two taxonomies, genres and writers for the post type "book"
function create_gift_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Product Types', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Product Type', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Product Types', 'textdomain' ),
        'all_items'         => __( 'All Product Types', 'textdomain' ),
        'parent_item'       => __( 'Parent Product Type', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Product Type:', 'textdomain' ),
        'edit_item'         => __( 'Edit Product Type', 'textdomain' ),
        'update_item'       => __( 'Update Product Type', 'textdomain' ),
        'add_new_item'      => __( 'Add New Product Type', 'textdomain' ),
        'new_item_name'     => __( 'New Product Type', 'textdomain' ),
        'menu_name'         => __( 'Product Type', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'product' ),
    );

    register_taxonomy( 'product', array( 'gifts_supplies' ), $args );

}

add_action( 'init', 'create_gift_taxonomies', 0 );

//**************************
// INTRANET MEMBER CPT
//**************************

function member() {

    $labels = array(
        'name'                  => _x( 'Members', 'Post Type General Name', 'tec' ),
        'singular_name'         => _x( 'Member', 'Post Type Singular Name', 'tec' ),
        'menu_name'             => __( 'Members', 'tec' ),
        'name_admin_bar'        => __( 'Members', 'tec' ),
        'archives'              => __( 'Member Archives', 'tec' ),
        'attributes'            => __( 'Member Attributes', 'tec' ),
        'parent_item_colon'     => __( 'Parent Member:', 'tec' ),
        'all_items'             => __( 'All Members', 'tec' ),
        'add_new_item'          => __( 'Add New Member', 'tec' ),
        'add_new'               => __( 'Add New', 'tec' ),
        'new_item'              => __( 'New Member', 'tec' ),
        'edit_item'             => __( 'Edit Member', 'tec' ),
        'update_item'           => __( 'Update Member', 'tec' ),
        'view_item'             => __( 'View Member', 'tec' ),
        'view_items'            => __( 'View Members', 'tec' ),
        'search_items'          => __( 'Search Members', 'tec' ),
        'not_found'             => __( 'Not found', 'tec' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'tec' ),
        'featured_image'        => __( 'Featured Image', 'tec' ),
        'set_featured_image'    => __( 'Set featured image', 'tec' ),
        'remove_featured_image' => __( 'Remove featured image', 'tec' ),
        'use_featured_image'    => __( 'Use as featured image', 'tec' ),
        'insert_into_item'      => __( 'Insert into Member', 'tec' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Member', 'tec' ),
        'items_list'            => __( 'Member list', 'tec' ),
        'items_list_navigation' => __( 'Member list navigation', 'tec' ),
        'filter_items_list'     => __( 'Filter Member list', 'tec' ),
    );
    $args = array(
        'label'                 => __( 'Members', 'tec' ),
        'description'           => __( 'Members part of TEC' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( 'tec_community', 'region', 'diocese', 'role' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 2,
        'menu_icon'             => 'dashicons-admin-users',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'rewrite'               => array('slug' => 'intranet/member','with_front' => false),
    );
    register_post_type( 'member', $args );

}
add_action( 'init', 'member', 0 );


// TAXONOMY

// TEC Community

function tec_community_taxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'TEC Communities', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'TEC Community', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search TEC Communities', 'textdomain' ),
        'all_items'         => __( 'All TEC Communities', 'textdomain' ),
        'parent_item'       => __( 'Parent TEC Community', 'textdomain' ),
        'parent_item_colon' => __( 'Parent TEC Community:', 'textdomain' ),
        'edit_item'         => __( 'Edit TEC Community', 'textdomain' ),
        'update_item'       => __( 'Update TEC Community', 'textdomain' ),
        'add_new_item'      => __( 'Add New TEC Community', 'textdomain' ),
        'new_item_name'     => __( 'New TEC Community', 'textdomain' ),
        'menu_name'         => __( 'TEC Community', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'tec_community' ),
    );

    register_taxonomy( 'tec_community', array( 'member' ), $args );

}

add_action( 'init', 'tec_community_taxonomy', 0 );


// Region

function region_taxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Regions', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Region', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Regions', 'textdomain' ),
        'all_items'         => __( 'All Regions', 'textdomain' ),
        'parent_item'       => __( 'Parent Region', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Region:', 'textdomain' ),
        'edit_item'         => __( 'Edit Region', 'textdomain' ),
        'update_item'       => __( 'Update Region', 'textdomain' ),
        'add_new_item'      => __( 'Add New Region', 'textdomain' ),
        'new_item_name'     => __( 'New Region', 'textdomain' ),
        'menu_name'         => __( 'Region', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'region' ),
    );

    register_taxonomy( 'region', array( 'member' ), $args );

}

add_action( 'init', 'region_taxonomy', 0 );


// Diocese

function diocese_taxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Dioceses', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Diocese', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Dioceses', 'textdomain' ),
        'all_items'         => __( 'All Dioceses', 'textdomain' ),
        'parent_item'       => __( 'Parent Diocese', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Diocese:', 'textdomain' ),
        'edit_item'         => __( 'Edit Diocese', 'textdomain' ),
        'update_item'       => __( 'Update Diocese', 'textdomain' ),
        'add_new_item'      => __( 'Add New Diocese', 'textdomain' ),
        'new_item_name'     => __( 'New Diocese', 'textdomain' ),
        'menu_name'         => __( 'Diocese', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'diocese' ),
    );

    register_taxonomy( 'diocese', array( 'member' ), $args );

}

add_action( 'init', 'diocese_taxonomy', 0 );

//
// Role

// function role_taxonomy() {
//     // Add new taxonomy, make it hierarchical (like categories)
//     $labels = array(
//         'name'              => _x( 'Roles', 'taxonomy general name', 'textdomain' ),
//         'singular_name'     => _x( 'Role', 'taxonomy singular name', 'textdomain' ),
//         'search_items'      => __( 'Search Roles', 'textdomain' ),
//         'all_items'         => __( 'All Roles', 'textdomain' ),
//         'parent_item'       => __( 'Parent Role', 'textdomain' ),
//         'parent_item_colon' => __( 'Parent Role:', 'textdomain' ),
//         'edit_item'         => __( 'Edit Role', 'textdomain' ),
//         'update_item'       => __( 'Update Role', 'textdomain' ),
//         'add_new_item'      => __( 'Add New Role', 'textdomain' ),
//         'new_item_name'     => __( 'New Role', 'textdomain' ),
//         'menu_name'         => __( 'Role', 'textdomain' ),
//     );

//     $args = array(
//         'hierarchical'      => true,
//         'labels'            => $labels,
//         'show_ui'           => true,
//         'show_admin_column' => true,
//         'query_var'         => true,
//         'rewrite'           => array( 'slug' => 'role' ),
//     );

//     register_taxonomy( 'role', array( 'member' ), $args );

// }

// add_action( 'init', 'role_taxonomy', 0 );
