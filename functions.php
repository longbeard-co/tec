<?php

// enqueue the child theme stylesheet

Function wp_schools_enqueue_scripts() {
//wp_register_style( 'style');
    wp_enqueue_style( 'main_css', plugin_dir_url( __FILE__ ) . 'style.css', array(), filemtime( plugin_dir_path(__FILE__) . 'style.css' ) );
    wp_register_script( 'revealJs', 'https://unpkg.com/scrollreveal/dist/scrollreveal.min.js', null, null, true );
wp_enqueue_script( 'flexibility', get_stylesheet_directory_uri() . '../../../plugins/nitrogen/assets/js/vendors/flexibility-master/flexibility.js', array( 'jquery' ), '1.0', true );

}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);
 
// get custom post types once
require_once('lb-custom-post-types.php');
// require_once('tribe-events/src/views/list/single-event.php');

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_4', 'pray_form_submit_button', 10, 2 );
function pray_form_submit_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_4'>Submit</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_1', 'newsletter_form_submit_button', 10, 2 );
function newsletter_form_submit_button( $button, $form ) {
    return "<button class='button white submit-button' id='gform_submit_button_1'>Subscribe</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_3', 'request_form_submit_button', 10, 2 );
function request_form_submit_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_3'>Request</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_6', 'team_contact_form_submit_button', 10, 2 );
function team_contact_form_submit_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_6'>Contact</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_8', 'connect_start_TEC_form_submit_button', 10, 2 );
function connect_start_TEC_form_submit_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_6'>Submit</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_9', 'connect_stay_connected_form_submit_button', 10, 2 );
function connect_stay_connected_form_submit_button( $button, $form ) {
    return "<button class='button white submit-button' id='gform_submit_button_9'>Subscribe</button>";
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button_10', 'pray_for_TEC_intentions_form_submit_button', 10, 2 );
function pray_for_TEC_intentions_form_submit_button( $button, $form ) {
    return "<button class='button submit-button' id='gform_submit_button_10'>Submit</button>";
}

// // filter the Gravity Forms button type
// add_filter( 'gform_submit_button_11', 'gifts_supplies_submit_button', 10, 2 );
// function gifts_supplies_submit_button( $button, $form ) {
//     return "<button class='button submit-button' id='gform_submit_button_10'>Submit</button>";
// }

// ENQUEUE WEB FONTS

function init_fonts() {
    $fonts = '<style type="text/css">

// @import url("https://fast.fonts.net/t/1.css?apiType=css&projectid=80e6ca02-3223-4e2a-a4ef-00cf8be96e72");
//     @font-face{
//         font-family:"AvenirNextLTW01-Regular";
//         src:url("/Fonts/e9167238-3b3f-4813-a04a-a384394eed42.eot?#iefix");
//         src:url("/Fonts/e9167238-3b3f-4813-a04a-a384394eed42.eot?#iefix") format("eot"),url("/Fonts/2cd55546-ec00-4af9-aeca-4a3cd186da53.woff2") format("woff2"),url("/Fonts/1e9892c0-6927-4412-9874-1b82801ba47a.woff") format("woff"),url("/Fonts/46cf1067-688d-4aab-b0f7-bd942af6efd8.ttf") format("truetype");
//     }
//     @font-face{
//         font-family:"AvenirNextLTW01-Italic";
//         src:url("/Fonts/d1fddef1-d940-4904-8f6c-17e809462301.eot?#iefix");
//         src:url("/Fonts/d1fddef1-d940-4904-8f6c-17e809462301.eot?#iefix") format("eot"),url("/Fonts/7377dbe6-f11a-4a05-b33c-bc8ce1f60f84.woff2") format("woff2"),url("/Fonts/92b66dbd-4201-4ac2-a605-4d4ffc8705cc.woff") format("woff"),url("/Fonts/18839597-afa8-4f0b-9abb-4a30262d0da8.ttf") format("truetype");
//     }
//     @font-face{
//         font-family:"Avenir Next LT W01 Bold";
//         src:url("/Fonts/dccb10af-07a2-404c-bfc7-7750e2716bc1.eot?#iefix");
//         src:url("/Fonts/dccb10af-07a2-404c-bfc7-7750e2716bc1.eot?#iefix") format("eot"),url("/Fonts/14c73713-e4df-4dba-933b-057feeac8dd1.woff2") format("woff2"),url("/Fonts/b8e906a1-f5e8-4bf1-8e80-82c646ca4d5f.woff") format("woff"),url("/Fonts/890bd988-5306-43ff-bd4b-922bc5ebdeb4.ttf") format("truetype");
//     }
//     @font-face{
//         font-family:"AvenirNextLTW01-BoldIta";
//         src:url("/Fonts/ac2d4349-4327-448f-9887-083a6a227a52.eot?#iefix");
//         src:url("/Fonts/ac2d4349-4327-448f-9887-083a6a227a52.eot?#iefix") format("eot"),url("/Fonts/eaafcb26-9296-4a57-83e4-4243abc03db7.woff2") format("woff2"),url("/Fonts/25e83bf5-47e3-4da7-98b1-755efffb0089.woff") format("woff"),url("/Fonts/4112ec87-6ded-438b-83cf-aaff98f7e987.ttf") format("truetype");
//     }
//     @font-face{
//         font-family:"Avenir Next LT W01 Demi";
//         src:url("/wp-content/plugins/nitrogen/assets/fonts/12d643f2-3899-49d5-a85b-ff430f5fad15.eot?#iefix");
//         src:url("/wp-content/plugins/nitrogen/assets/fonts/12d643f2-3899-49d5-a85b-ff430f5fad15.eot?#iefix") format("eot"),url("/wp-content/plugins/nitrogen/assets/fonts/aad99a1f-7917-4dd6-bbb5-b07cedbff64f.woff2") format("woff2"),url("/wp-content/plugins/nitrogen/assets/fonts/91b50bbb-9aa1-4d54-9159-ec6f19d14a7c.woff") format("woff"),url("/wp-content/plugins/nitrogen/assets/fonts/a0f4c2f9-8a42-4786-ad00-fce42b57b148.ttf") format("truetype");
//     }
    
    @import url("https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600;900&display=swap");

</style>';
    
echo "\n" . $fonts;
}

add_action('wp_head', 'init_fonts', 35);



// change the other choice option on Support TEC page.
// add_filter( 'gform_other_choice_value', 'set_placeholder', 10, 2 );
function set_placeholder( $placeholder, $field ) {
    return 'General/Other';
}


// add lb_ classes to all applicable VC elements
add_action( 'vc_after_init', 'add_lb_custom_class' );/* Note: here we are using vc_after_init because WPBMap::GetParam and mutateParame are available only when default content elements are "mapped" into the system */
function add_lb_custom_class() {
    //Get array of vc elements minus 'vc_' that support custom class "el_class"
    $elements = array(
        'vc_column_text'    => 'lb-column_text',
        'vc_empty_space'    => 'lb-empty_space',
        'vc_row'            => 'lb-row',
        'vc_column'         => 'lb-column',
        'vc_row_inner'      => 'lb-row_inner',
        'vc_column_inner'   => 'lb-column_inner'
        );

    //Loop over each element in array
    foreach ($elements as $oldEl => $newClass) {
        //Get current values stored in the class param in element
        $param = WPBMap::getParam( $oldEl, 'el_class' );
        //Append new value to the 'value' array
        $param['value'][__( 'el_class', $newClass )] = $newClass;
        //Finally "mutate" param with new values
        vc_update_shortcode_param( $oldEl, $param );
    }
}


// add_action( 'tribe_events_single_event_after_the_content', 'forum_1020203_custom_content' );

// function forum_1020203_custom_content() {
//   $output = '<h1>hello world</h1>';
//   echo $output;
// }

add_action( 'ct_before_builder', 'custom_reset_the_query' );
/**
 * Fix the query issue with The Events Calendar.
 */
function custom_reset_the_query() {
    wp_reset_query();
}


function mlp_display_parts_change($displayParts){
    return array('search','map','message','list','paging');
}

add_filter( 'mlp_display_parts', 'mlp_display_parts_change' );

add_action( 'init', 'create_country_taxonomy', 0 );

function create_country_taxonomy(){
    $catargsCountry = array(
        'labels' => array(
            'label' => __( 'Countries' ),
            'rewrite' => array( 'slug' => 'country' ),
            'hierarchical' => true,
            ),
        'label' => "Country",
        'public' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );
  
    register_taxonomy( 'map_location_categories_country', array('maplist'), $catargsCountry );
}

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_state_taxonomy', 0 );
 
function create_state_taxonomy(){
    $catargsState = array(
        'labels' => array(
            'label' => __( 'States' ),
            'rewrite' => array( 'slug' => 'state' ),
            'hierarchical' => true,
            ),
        'label' => "State",
        'public' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );
  
    register_taxonomy( 'map_location_categories_state', array('maplist'), $catargsState );
}

function tribe_custom_theme_text ( $translation, $text, $domain ) {
 
    // Put your custom text here in a key => value pair
    // Example: 'Text you want to change' => 'This is what it will be changed to'
    // The text you want to change is the key, and it is case-sensitive
    // The text you want to change it to is the value
    // You can freely add or remove key => values, but make sure to separate them with a comma
    // This example changes the label "Venue" to "Location", and "Related Events" to "Similar Events"
    $custom_text = array(
        'Next %s' => 'Upcoming %s'
    );
 
    // If this text domain starts with "tribe-", "the-events-", or "event-" and we have replacement text
        if( (strpos($domain, 'tribe-') === 0 || strpos($domain, 'the-events-') === 0 || strpos($domain, 'event-') === 0) && array_key_exists($translation, $custom_text) ) {
        $translation = $custom_text[$translation];
    }
    return $translation;
}
add_filter('gettext', 'tribe_custom_theme_text', 20, 3);

/*
 * Alters event's archive titles
 */
function tribe_alter_event_archive_titles ( $original_recipe_title, $depth ) {
    // Modify the titles here
    // Some of these include %1$s and %2$s, these will be replaced with relevant dates
    $title_upcoming =   'TEC CALENDAR'; // List View: Upcoming events
    $title_past =       'Past Events'; // List view: Past events
    $title_range =      'Events for %1$s - %2$s'; // List view: range of dates being viewed
    $title_month =      'Events for %1$s'; // Month View, %1$s = the name of the month
    $title_day =        'Events for %1$s'; // Day View, %1$s = the day
    $title_all =        'All events for %s'; // showing all recurrences of an event, %s = event title
    $title_week =       'Events for week of %s'; // Week view
    // Don't modify anything below this unless you know what it does
    global $wp_query;
    $tribe_ecp = Tribe__Events__Main::instance();
    $date_format = apply_filters( 'tribe_events_pro_page_title_date_format', tribe_get_date_format( true ) );
    // Default Title
    $title = $title_upcoming;
    // If there's a date selected in the tribe bar, show the date range of the currently showing events
    if ( isset( $_REQUEST['tribe-bar-date'] ) && $wp_query->have_posts() ) {
        if ( $wp_query->get( 'paged' ) > 1 ) {
            // if we're on page 1, show the selected tribe-bar-date as the first date in the range
            $first_event_date = tribe_get_start_date( $wp_query->posts[0], false );
        } else {
            //otherwise show the start date of the first event in the results
            $first_event_date = tribe_event_format_date( $_REQUEST['tribe-bar-date'], false );
        }
        $last_event_date = tribe_get_end_date( $wp_query->posts[ count( $wp_query->posts ) - 1 ], false );
        $title = sprintf( $title_range, $first_event_date, $last_event_date );
    } elseif ( tribe_is_past() ) {
        $title = $title_past;
    }
    // Month view title
    if ( tribe_is_month() ) {
        $title = sprintf(
            $title_month,
            date_i18n( tribe_get_option( 'monthAndYearFormat', 'F Y' ), strtotime( tribe_get_month_view_date() ) )
        );
    }
    // Day view title
    if ( tribe_is_day() ) {
        $title = sprintf(
            $title_day,
            date_i18n( tribe_get_date_format( true ), strtotime( $wp_query->get( 'start_date' ) ) )
        );
    }
    // All recurrences of an event
    if ( function_exists('tribe_is_showing_all') && tribe_is_showing_all() ) {
        $title = sprintf( $title_all, get_the_title() );
    }
    // Week view title
    if ( function_exists('tribe_is_week') && tribe_is_week() ) {
        $title = sprintf(
            $title_week,
            date_i18n( $date_format, strtotime( tribe_get_first_week_day( $wp_query->get( 'start_date' ) ) ) )
        );
    }
    if ( is_tax( $tribe_ecp->get_event_taxonomy() ) && $depth ) {
        $cat = get_queried_object();
        // $title = '<a href="' . esc_url( tribe_get_events_link() ) . '">' . $title . '</a>';
        $title = '<div class="parent">' . $title . "  &#8250;</div>";
        // $title .= ' &#8250; ' . $cat->name;
        $title .= $cat->name;
    }
    return $title;
}
add_filter( 'tribe_get_events_title', 'tribe_alter_event_archive_titles', 11, 2 );


/*
 * The Events Calendar Remove Events from Month and List Views
 * add coding to theme's functions.php
 * @version 3.12
 * modify here with event category slugs: array( 'concert', 'convention' )
*/
add_action( 'pre_get_posts', 'tribe_exclude_events_category_month_list' );
function tribe_exclude_events_category_month_list( $query ) {

    if ( isset( $query->query_vars['eventDisplay'] ) && ! is_singular( 'tribe_events' ) && $query->query_vars['tribe_events_cat'] != 'intranet' ) {

        if ( $query->query_vars['eventDisplay'] == 'list' && ! is_tax( Tribe__Events__Main::TAXONOMY ) || $query->query_vars['eventDisplay'] == 'month' && $query->query_vars['post_type'] == Tribe__Events__Main::POSTTYPE && ! is_tax( Tribe__Events__Main::TAXONOMY ) && empty( $query->query_vars['suppress_filters'] ) ) {

            $query->set( 'tax_query', array(

                array(
                    'taxonomy' => Tribe__Events__Main::TAXONOMY,
                    'field'    => 'slug',
                    'terms'    => array( 'intranet' ),
                    'operator' => 'NOT IN'
                )
            ) );
        }

    }

    return $query;
}

//#########################
// INTRANET
//#########################


// VALIDATION

// *uncomment on production
add_action( 'template_redirect', 'intranet_redirect' );

function intranet_redirect() {
    $post = get_post();
    $postID = get_the_ID();

   if( $postID == '1945') { } else if ( is_page( 'intranet' ) || '1366' == $post->post_parent || '1745' == $post->post_parent) {

        // 1745 = id page of /intranet parent page
        // 1366 = id page of forum

        if( !is_user_logged_in() ) {
            $redirect = get_permalink();
            wp_redirect( wp_login_url( $redirect ) );
        }

        if ( is_user_logged_in() ) {
            $user = wp_get_current_user();
            $valid_roles = [ 'administrator', 'staff' ];
            $pending_role = [ 'pending_approval' ];

            $the_roles = array_intersect( $valid_roles, $user->roles );

            $pending_roles = array_intersect( $pending_role, $user->roles );

            // Send pending approvals to splash page
            if( in_array('pending_approval', $user->roles ) ) {
                wp_redirect('https://tecconference.org/pending-approval');
                exit;
            } elseif ( empty( $the_roles ) ) {
                wp_redirect( home_url() );
                exit;
            }
            // The current user does not have any of the 'valid' roles.

        }
    }
}


// FORMS

$register_form      = '31';
$edit_profile_form  = '32';

$member_taxonomies  = array(
    0 => array(
        'slug'      => 'tec_community',
        'field_id'  => 8,
        'label'     => 'TEC Community'
    ),
    1 => array(
        'slug'      => 'region',
        'field_id'  => 9,
        'label'     => 'Region'
    ),
    2 => array(
        'slug'      => 'diocese',
        'field_id'  => 10,
        'label'     => 'Diocese'
    ),
    3 => array(
        'slug'      => 'role',
        'field_id'  => 7,
        'label'     => 'Role'
    ),
);

// GF | Populate Dynamically from Taxonomy

// $taxonomy_names = array( 'tec_community', 'region', 'diocese', 'role' );
$forms_to_populate = array( $register_form, $edit_profile_form );

foreach ($forms_to_populate as $form) {

    // foreach ($taxonomy_names as $taxonomy) {
    //     add_filter( 'gform_pre_render_'.$form, 'populate_checkbox_'.$taxonomy );
    //     add_filter( 'gform_pre_validation_'.$form, 'populate_checkbox_'.$taxonomy );
    //     add_filter( 'gform_pre_submission_filter_'.$form, 'populate_checkbox_'.$taxonomy );
    //     add_filter( 'gform_admin_pre_render_'.$form, 'populate_checkbox_'.$taxonomy );
    // }

    add_filter( 'gform_pre_render_'.$form, 'populate_taxonomy' );
    add_filter( 'gform_pre_validation_'.$form, 'populate_taxonomy' );
    add_filter( 'gform_pre_submission_filter_'.$form, 'populate_taxonomy' );
    add_filter( 'gform_admin_pre_render_'.$form, 'populate_taxonomy' );
}

function populate_taxonomy( $form ) {
    global $member_taxonomies;

    foreach( $member_taxonomies as $key => $taxonomy ) {
        foreach( $form['fields'] as $field )  {

            $field_id = $taxonomy['field_id'];
            if ( $field->id != $field_id ) {
                continue;
            }

            if ($field_id == 7) {  // Role - Asgaros Usergroup
                $terms = AsgarosForumUserGroups::getUserGroups();
     
                $input_id = 1;
                foreach( $terms as $term ) {
         
                    //skipping index that are multiples of 10 (multiples of 10 create problems as the input IDs)
                    if ( $input_id % 10 == 0 ) {
                        $input_id++;
                    }

                    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
                    $inputs[] = array( 'label' => $term->name, 'id' => "{$field_id}.{$input_id}" );
         
                    $input_id++;
                }
         
                $field->choices = $choices;
                $field->inputs = $inputs;
            } else { // Custom Taxonomoies
                // you can add additional parameters here to alter the posts that are retreieved
                // more info: http://codex.wordpress.org/Template_Tags/get_posts
                $terms = get_terms( $taxonomy['slug'] , 'hide_empty=0&orderby=none' );
                 
                $choices = array();
         
                foreach ( $terms as $term ) {
                    $choices[] = array( 'text' => $term->name, 'value' => $term->name );
                }
         
                // update 'Select a Post' to whatever you'd like the instructive option to be
                $field->placeholder = 'Select Your ' . $taxonomy['label'];
                $field->choices = $choices;
            }
        }
    }

    return $form;
}


//########################
// PRE-POPULATE FORM
//

// VARIABLES

// Get User
function current_user() {
    $current_user = wp_get_current_user(); // LIVE
    // $current_user = get_user_by( 'id', 34 ); // TEST - johndoe

    return $current_user;
}

// enable custom url query 'uid'
add_filter( 'query_vars', 'themeslug_query_vars' );
function themeslug_query_vars( $qvars ) {
    $qvars[] = 'uid';
    $qvars[] = 'profile_updated';
    return $qvars;
}

// Get Member Post ID
function post_update_id() {
    $current_user = current_user();

    $args = array(
        'author'        =>  $current_user->ID,
        'post_type'     =>  'member',
        'numberposts'   =>  1,
    );

    $posts = get_posts( $args );

    foreach ( $posts as $post ) {
        $pid = $post->ID;
    }

    return $pid;
}


// POPULATE Form

$field_names = array('salutation', 'first_name', 'last_name', 'casual_name', 'primary_email', 'secondary_email', 'phone', 'phone_other', 'street_address', 'city', 'us_state', 'zip', 'country', 'entry_id', 'tec_community', 'region', 'diocese', 'role', 'username');

foreach ($field_names as $name) {
  add_filter('gform_field_value_'.$name, 'populate_custom_field');
}

function populate_custom_field( $value ) {

    // get the current filter that was called
    // it will contain the field name
    $filter = current_filter();
    // remove 'gform_field_value_' from filter name
    $field  = str_replace('gform_field_value_', '', $filter);

    $pid    = post_update_id();
    $value  = get_field( $field, $pid );

    if( empty( $value ) ) {

        $taxonomies = get_post_taxonomies( $pid );

        if (in_array($field, $taxonomies)) {
            foreach ($taxonomies as $taxonomy) {
                $value = wp_get_post_terms( $pid, $field, array( 'fields' => 'names' ) );
            }
        } elseif ($field === 'username') {
            $current_user = current_user();
            $value = $current_user->user_login;
        } elseif ($field === 'role') {
            $current_user = current_user();
            $value = AsgarosForumUserGroups::getUserGroupsOfUser( $current_user->ID, 'ids' );
        }
    }

    // Restore original post data.
    wp_reset_postdata();

    return $value;
}

// Pre-populate Author
add_action( 'gform_after_submission_'.$register_form, 'set_post_author', 10, 2 );
function set_post_author( $entry, $form ) {

    //if the Advanced Post Creation add-on is used, more than one post may be created for a form submission
    //the post ids are stored as an array in the entry meta
    $created_posts = gform_get_meta( $entry['id'], 'gravityformsadvancedpostcreation_post_id' );
    foreach ( $created_posts as $post )
    {
        $post_id = $post['post_id']; // use just 1 post_id
    }
    
    // get email from Registration form
    $registered_email = rgar( $entry, '11' );
    
    // get user with registered email
    $user = get_user_by( 'email', $registered_email );
    
    // if user exists ...
    if ( $user ):
        
        // get user ID
        $user_id = $user->ID;
        
        // update post author
        $arg = array(
            'ID' => $post_id,
            'post_type'   =>  'member',
            'post_author' => $user_id,
        );
        wp_update_post( $arg );
        
    endif;
}


// READONLY SCRIPT for USERNAME
add_filter( 'gform_pre_render_'.$edit_profile_form, 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>
 
    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a input with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>
 
    <?php
    return $form;
}

// EDIT FORM - Update Data

add_action( 'gform_after_submission_'.$edit_profile_form, 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {

    // ACF Custom Fields
    $fields = array(
        'salutation'        => '1_2', 
        'first_name'        => '1_3', 
        'last_name'         => '1_6', 
        'casual_name'       => '3', 
        'primary_email'     => '11', 
        'secondary_email'   => '15', 
        'phone'             => '12', 
        'phone_other'       => '13', 
        'street_address'    => '17', 
        'city'              => '18', 
        'us_state'          => '20', 
        'zip'               => '19', 
        'country'           => '21',
        'entry_id'          => '25'
    );

    // getting post
    // $post = get_post( $entry['post_id'] );
    $post = get_post( post_update_id() );

    // changing post content

    // updating post
    $post_to_update = wp_update_post( $post );

    // Custom Taxonomies
    $taxonomies = array(
        'tec_community' => '8',
        'region'        => '9',
        'diocese'       => '10'
        // 'role'        => '7'
    );

    // Update Taxonomies
    foreach ( $taxonomies as $taxonomy => $input_id ) {

        $name = 'input_' . $input_id; 
        if ( ! empty( $_POST[$name] ) ) {
            $selected[] = $_POST[$name];
        }

        // for Checkbox, add to query
        for ( $i = 0; $i < 10; $i++ ) {
            $name = 'input_' . $input_id . '_' . $i;
            if ( ! empty( $_POST[$name] ) ) {
                $selected[] = $_POST[$name];
            }
        }

        // sanitize array
        $selected = array_map( 'sanitize_text_field', $selected );

        wp_set_object_terms( $post_to_update, $selected, $taxonomy, false );

        // clear array
        unset( $selected );
    }
    
    // Update ACF Fields
    foreach ($fields as $field => $input_id) {

        $value = $_POST['input_' . $input_id];

        // clean up phone number before saving
        if ( $field === 'phone' || $field === 'phone_other' ) {
            preg_replace('/[^0-9]/', '', $value);
        }

        // sanitize email
        if ( $field === 'primary_email' || $field === 'secondary_email' ) {
            $value = sanitize_email( $value );
        }

        // sanitize string
        if ( is_string( $value ) ) {
            $value = sanitize_text_field( $value );
        }

        update_field( $field, $value, $post_to_update );
    }

    // Featured Image
    $file_input_id = 26;
    $upload_path = rgar( $entry, $file_input_id );

    if ( ! empty( $upload_path ) ) {

        // Build the $file_array with
        // $upload_path = the upload path of the image 
        // $temp = storing the image in wordpress
        
        $upload_path = $upload_path;
         
        $tmp = download_url( $upload_path );
         
        $file_array = array(
            'name' => basename( $upload_path ),
            'tmp_name' => $tmp
        );
         
        // Check for download errors
        // if there are error unlink the temp file name

        if ( is_wp_error( $tmp ) ) {
            @unlink( $file_array[ 'tmp_name' ] );
            return $tmp;
        }
         
        // now we can actually use media_handle_sideload
        // we pass it the file array of the file to handle
        // and the post id of the post to attach it to
        // $post_id can be set to '0' to not attach it to any particular post
         
        $attachment_id = media_handle_sideload( $file_array, $post_to_update );
         
        // We don't want to pass something to $attachment_id if there were upload errors.
        // so, this checks for errors

        if ( is_wp_error( $attachment_id ) ) {
            @unlink( $file_array['tmp_name'] );
            return $attachment_id;
        }

        // delete old attachment
        // $old_attachment_id = get_post_thumbnail_id( $post_to_update );
        // if ( $old_attachment_id ) {
        //     wp_delete_attachment( $old_attachment_id, true );
        // }
         
        set_post_thumbnail( $post_to_update, $attachment_id );
        
    }

    // Update User
    // Get user info
    $current_user = current_user();

    $first_name   = $_POST['input_1_3'];
    $last_name    = $_POST['input_1_6'];
    $casual_name  = $_POST['input_3'];

    if ( ! empty( $first_name ) ) {
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $first_name ) );
    }

    if ( ! empty( $last_name ) ) {
        update_user_meta( $current_user->ID, 'last_name', esc_attr( $last_name ) );
    }

    if ( ! empty( $casual_name ) ) {
        update_user_meta( $current_user->ID, 'nickname', esc_attr( $casual_name ) );
    }

    // Update Display Name
    $display_name = esc_attr( $first_name . ' ' . $last_name );
    wp_update_user( array('ID' => $current_user->ID, 'display_name' =>  $display_name) );
    
    // Update Password
    $password = $_POST['input_24'];
    $password2 = $_POST['input_24_2'];

    if ( ! empty( $password ) && ! empty( $password2 ) ) {
        if ( $password == $password2 ) {
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $password ) ) );
        }
    }
}

// VALIDATE EMAIL
// Since we are using the gravity forms post fields, we can't use the default gravity forms email validation
add_filter('gform_validation_'.$edit_profile_form, 'email_validation');
function email_validation($validation_result){
    // Set the $validation_result object to $form for clarity
    $form = $validation_result["form"];
    // Get the value the user entered for the email field
    // EDIT HERE WITH THE INPUT NAME OF YOUR FIELD (input_2 is the name attribute of the input field)
    $email = $_POST["input_11"];
    $current_user = current_user();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // set the form validation to false
        $validation_result["is_valid"] = false;
        // specify the invalid field and provide custom validation message
        // EDIT HERE WITH THE ZERO INDEXED NUMBER OF THE FIELD (in thise case $form["fields"][2], because the field is the third field in the form)
        $form["fields"][2]["failed_validation"] = true;
        // $form["fields"][2]["validation_message"] = 'Please enter a valid email address';
    } elseif ( email_exists( esc_attr( $email ) ) != $current_user->id ) {
        $validation_result["is_valid"] = false;
        $form["fields"][2]["failed_validation"] = true;
        $form["fields"][2]["validation_message"] = 'This email is already used by another user. Please try a different one.';
    } else {
        wp_update_user( array('ID' => $current_user->ID, 'user_email' => esc_attr( $email )));
        // update_user_meta( $current_user->ID, 'user_email', esc_attr( $email ) );
    }
    // update the form in the validation result with the form object you modified
    $validation_result["form"] = $form;
    return $validation_result;
}


// REMOVE FORM ENTRY After Submission
// To prevent bloat

add_action( 'gform_after_submission_'.$edit_profile_form, 'remove_form_entry' );
function remove_form_entry( $entry ) {
    GFAPI::delete_entry( $entry['id'] );
}

// Add TEC Staff on User Dropdown for changing users
function add_staff_to_dropdown( $query_args, $r ) {
  
    $query_arg['who'] = 'staff';
    return $query_arg;
}
add_filter( 'wp_dropdown_users_args', 'add_staff_to_dropdown', 10, 2 );


// ACF Pre-Populate

// Populate ACF User Group / Role from Asgaros Usergroups
add_filter('acf/load_field/name=role', 'acf_load_user_group_field_choices');

function acf_load_user_group_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // get Asgaros Forum Roles
    $roles = AsgarosForumUserGroups::getUserGroups();

    foreach ($roles as $role) {

        $value = $role->term_id;
        $label = $role->name;

        // append to choices
        $field['choices'][ $value ] = $label;
    }
            
    // return the field
    return $field;
    
}

// On Profile / Member Update
// Synchronize Asgaros Usergroups with ACF Role Field for searchability

// - Update User -> update ACF Role

add_action( 'profile_update', 'assign_roles_to_member', 10, 2 );

function assign_roles_to_member( $user_id, $old_user_data ) {

    $args = array(
        'author'        =>  $user_id,
        'post_type'     =>  'member',
        'numberposts'   =>  1,
    );
    $posts = get_posts( $args );

    foreach ($posts as $post) {
        
        $field = 'role';
        $value = AsgarosForumUserGroups::getUserGroupsOfUser( $user_id, 'ids' );
        $post_to_update = wp_update_post( get_post( $post->ID ) );

        update_field($field, $value, $post_to_update);
        AsgarosForumUserGroups::insertUserGroupsOfUsers( $user_id, $value );
    }
}

// - Update Member -> update User Role

add_action('acf/save_post', 'my_acf_save_post', 15);
add_action('pmxi_saved_post', 'my_acf_save_post', 10, 1); // After WP All Import post creation

function my_acf_save_post( $post_id ) {

    $pid   = $post_id;

    if ( get_post_type( $pid ) == 'member' ) {

        $email = get_field( 'primary_email', $pid );

        // find related user by email
        $user  = get_user_by( 'email', $email );

        // if user is found, edit Asgaros Forum Role
        if ( $user ) {
            $selected = get_field( 'role', $pid );
            $selected = array_map( 'intval', $selected );
            AsgarosForumUserGroups::insertUserGroupsOfUsers( $user->ID, $selected );
        }

    }
}


// GRAVATAR / AVATAR 

add_filter( 'get_avatar' , 'my_custom_avatar' , 11, 5 ); 
function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;

    if ( is_numeric( $id_or_email ) ) {

        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );   
    }
    

    if ( $user && is_object( $user ) ) {

        // Define defaults

        $placeholder = '/wp-content/uploads/2019/12/TEC_User_Placeholder.jpg';

        $default = get_avatar_url( $id_or_email ) ? get_avatar_url( $id_or_email ) : $placeholder;

        // Check if User has a Member CPT
        $args = array(
            'author'        =>  $user->ID,
            'post_type'     =>  'member',
            'numberposts'   =>  1,
        );
        $posts = get_posts( $args );

        // If User has a Member CPT
        if ( ! empty( $posts ) ) {

            // Get Member Featured Image URL
            foreach ($posts as $post) {
                $avatar_url = get_the_post_thumbnail_url($post->ID, array(200,200));
            }
            
            // If Featured Image is not defined, revert to default
            if ( empty( $avatar_url ) ) {
                $avatar_url = $default;
            }

        } else {
            $avatar_url = $default;
        }

        $avatar = "<img alt='{$alt}' src='{$avatar_url}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
    
    }
    
    return $avatar;
}

// ASGAROS FORUM
add_filter('asgarosforum_filter_profile_link', 'my_profile_url', 10, 2);
function my_profile_url($profile_url, $user_object) {
    return home_url('/profile/?uid='.$user_object->ID);
}